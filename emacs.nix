/*
This is a nix expression to build Emacs and some Emacs packages I like
from source on any distribution where Nix is installed. This will install
all the dependencies from the nixpkgs repository and build the binary files.
*/

{ pkgs ? import <nixpkgs> {} }: 

let
  myEmacs = (pkgs.emacs.override {
    # Use gtk3 instead of the default gtk2
    withGTK3 = true;
    withGTK2 = false;
  });
  emacsWithPackages = (pkgs.emacsPackagesGen myEmacs).emacsWithPackages; 
in
  emacsWithPackages (epkgs: (with epkgs.melpaStablePackages; [ 
  ]) ++ (with epkgs.melpaPackages; [

    use-package
    
    ledger-mode

    magit
    git-timemachine
    diff-hl

    nix-mode
    
    ivy
    pdf-tools
    restart-emacs
    hydra
    yasnippet
    yasnippet-snippets
    fcitx
    
    
    org-bullets
    which-key
    ace-window
    anzu
    ace-window
    company
    flycheck flycheck-ledger

        
    monokai-theme

    spaceline
    
  ]) ++ (with epkgs.elpaPackages; [
    auctex
  ])++ (with epkgs.orgPackages; [
    org
  ])++ [
        pkgs.notmuch   # From main packages set

  ])
